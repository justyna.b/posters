# Posters

The PDF posters presented at conferences & meetings can be found in this directory.

Related publications:

Current manuscript in preparation (will be updated soon).

de Steenhuijsen Piters WAA, Binkowska J, Bogaert D. **Early Life Microbiota and Respiratory Tract Infections**. Cell Host Microbe. 2020 Aug 12;28(2):223-232. doi: 10.1016/j.chom.2020.07.004. PMID: 32791114.

Boardman JP, Hall J, Thrippleton MJ, Reynolds RM, Bogaert D, Davidson DJ, Schwarze J, Drake AJ, Chandran S, Bastin ME, Fletcher-Watson S. **Impact of preterm birth on brain development and long-term outcome: protocol for a cohort study in Scotland.** BMJ Open. 2020 Mar 4;10(3):e035854. doi: 10.1136/bmjopen-2019-035854. PMID: 32139495; PMCID: PMC7059503.

Contact details:
justyna.binkowska@ed.ac.uk

